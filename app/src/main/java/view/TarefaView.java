/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package view;

import controller.TarefaController;
import model.Projetos;
import model.Tarefas;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;


import com.toedter.calendar.JCalendar;


/**
 *
 * @author Lucas Matos
 */

public class TarefaView extends javax.swing.JDialog {

	private TarefaController controller;
	private Projetos projeto;
	private Tarefas tarefa;


	public TarefaView(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		initComponents();

		controller = new TarefaController();
		

	}

	TarefaView(Tarefas tarefa) {
		initComponents();

		controller = new TarefaController();
	}

	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed" desc="Generated
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField2 = new javax.swing.JTextField();
        jPanelToolBar = new javax.swing.JPanel();
        jLabelToolBarTitulo = new javax.swing.JLabel();
        jPanelTarefa = new javax.swing.JPanel();
        jLabelNome = new javax.swing.JLabel();
        jTextFieldNome = new javax.swing.JTextField();
        jLabelDescricao = new javax.swing.JLabel();
        jScrollPaneDescricao = new javax.swing.JScrollPane();
        jTextAreaDescricao = new javax.swing.JTextArea();
        jLabelDeadline = new javax.swing.JLabel();
        jButtonCancelar = new javax.swing.JButton();
        jButtonSalvar = new javax.swing.JButton();
        jButtonEditar = new javax.swing.JButton();
        jButtonCalendario = new javax.swing.JButton();
        jTextFieldData = new javax.swing.JTextField();

        jTextField2.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanelToolBar.setBackground(new java.awt.Color(51, 153, 255));

        jLabelToolBarTitulo.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabelToolBarTitulo.setForeground(java.awt.Color.white);
        jLabelToolBarTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelToolBarTitulo.setText("Tarefa");

        javax.swing.GroupLayout jPanelToolBarLayout = new javax.swing.GroupLayout(jPanelToolBar);
        jPanelToolBar.setLayout(jPanelToolBarLayout);
        jPanelToolBarLayout.setHorizontalGroup(
            jPanelToolBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelToolBarLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelToolBarTitulo)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelToolBarLayout.setVerticalGroup(
            jPanelToolBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelToolBarLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelToolBarTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, 66, Short.MAX_VALUE))
        );

        jPanelTarefa.setBackground(java.awt.Color.white);

        jLabelNome.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabelNome.setText("Nome");

        jTextFieldNome.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jTextFieldNome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldNomeActionPerformed(evt);
            }
        });

        jLabelDescricao.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabelDescricao.setText("Descrição");

        jTextAreaDescricao.setColumns(20);
        jTextAreaDescricao.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jTextAreaDescricao.setRows(5);
        jScrollPaneDescricao.setViewportView(jTextAreaDescricao);

        jLabelDeadline.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabelDeadline.setText("Prazo");

        jButtonCancelar.setForeground(new java.awt.Color(255, 51, 51));
        jButtonCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/excluir.png"))); // NOI18N
        jButtonCancelar.setText("CANCELAR");

        jButtonSalvar.setForeground(new java.awt.Color(51, 153, 255));
        jButtonSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/salvar.png"))); // NOI18N
        jButtonSalvar.setText("SALVAR");
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarActionPerformed(evt);
            }
        });

        jButtonEditar.setForeground(new java.awt.Color(51, 153, 255));
        jButtonEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/editar.png"))); // NOI18N
        jButtonEditar.setText("EDITAR");
        jButtonEditar.setToolTipText("");
        jButtonEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEditarActionPerformed(evt);
            }
        });

        jButtonCalendario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/calendario.png"))); // NOI18N
        jButtonCalendario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCalendarioActionPerformed(evt);
            }
        });

        jTextFieldData.setEditable(false);
        jTextFieldData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldDataActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelTarefaLayout = new javax.swing.GroupLayout(jPanelTarefa);
        jPanelTarefa.setLayout(jPanelTarefaLayout);
        jPanelTarefaLayout.setHorizontalGroup(
            jPanelTarefaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelTarefaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelTarefaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelNome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTextFieldNome)
                    .addComponent(jLabelDescricao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPaneDescricao)
                    .addGroup(jPanelTarefaLayout.createSequentialGroup()
                        .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButtonEditar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(10, 10, 10)
                        .addComponent(jButtonCancelar))
                    .addGroup(jPanelTarefaLayout.createSequentialGroup()
                        .addGroup(jPanelTarefaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelDeadline, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldData, javax.swing.GroupLayout.PREFERRED_SIZE, 323, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(12, 12, 12)
                        .addComponent(jButtonCalendario)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanelTarefaLayout.setVerticalGroup(
            jPanelTarefaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelTarefaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelNome)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelDescricao)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPaneDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanelTarefaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelTarefaLayout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(jLabelDeadline)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextFieldData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelTarefaLayout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(jButtonCalendario)))
                .addGap(41, 41, 41)
                .addGroup(jPanelTarefaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelTarefaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButtonCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButtonEditar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanelToolBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanelTarefa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanelToolBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelTarefa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void jTextFieldNomeActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jTextFieldNameActionPerformed
		// TODO add your handling code here:
	}// GEN-LAST:event_jTextFieldNameActionPerformed

	private void jButtonCalendarioActionPerformed(ActionEvent e) {
		// TODO add your handling code here:
		
         Frame frame = new JFrame("Aplicativo de Calendário");
	    ((JFrame) frame).setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.setSize(400, 300);
	    frame.setLayout(new BorderLayout());

	    JTextField textField = new JTextField(20);
	    frame.add(textField, BorderLayout.NORTH);

	    JDialog calendarDialog = new JDialog(frame, "Calendário", true);
	    calendarDialog.setSize(400, 300);
	    calendarDialog.setLayout(new BorderLayout());

	    
	    Calendar hoje = Calendar.getInstance();
	    JCalendar calendar = new JCalendar(hoje);

	    
	    calendar.setMinSelectableDate(hoje.getTime());

	    calendarDialog.add(calendar, BorderLayout.CENTER);

	    JButton selectDateButton = new JButton("Selecionar Data");
	    calendarDialog.add(selectDateButton, BorderLayout.SOUTH);

	    selectDateButton.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent e) {

	            Date selectedDate = calendar.getDate();

	            Calendar calendar = Calendar.getInstance();
	            calendar.setTime(selectedDate);

	            int day = calendar.get(Calendar.DAY_OF_MONTH);
	            int month = calendar.get(Calendar.MONTH) + 1;
	            int year = calendar.get(Calendar.YEAR);
	           
	            jTextFieldData.setText(day + "/" + month + "/" + year);

	            calendarDialog.dispose();
	        }
	    });

	    calendarDialog.setVisible(true);
	}

	private void jButtonSalvarActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButtonSalvarActionPerformed
		// TODO add your handling code here:
		try {
			if (jTextFieldNome.getText().isEmpty() || jTextFieldData.getText().isEmpty()) {
				JOptionPane.showMessageDialog(this,
						"A tarefa não foi salva pois existem campos a serem preenchidos!","Erro",
	            		JOptionPane.ERROR_MESSAGE);
			} else {

			
				Tarefas tarefa = new Tarefas();
				tarefa.setIdProjeto(projeto.getId());
				tarefa.setNome(jTextFieldNome.getText());
				tarefa.setDescricao(jTextAreaDescricao.getText());
				tarefa.setIsCompleto(false);

				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
				Date deadline = null;
				deadline = dateFormat.parse(jTextFieldData.getText());
				String dataFormatada = dateFormat.format(deadline);
			
				tarefa.setDeadline(dataFormatada);

				controller.create(tarefa);
				JOptionPane.showMessageDialog(this, "Tarefa salva com sucesso!");
				this.dispose();
				
			}

		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
	}// GEN-LAST:event_jButtonSalvarActionPerformed

	private void jButtonEditarActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButtonEditarActionPerformed
		// TODO add your handling code here:

		try {
			 if (tarefa == null) {
		            JOptionPane.showMessageDialog(this, "A tarefa não pode ser editada porque o ID da tarefa é nulo.");
			 }
			 else if (jTextFieldNome.getText().isEmpty()|| jTextFieldData.getText().isEmpty()) {
				JOptionPane.showMessageDialog(this,
						"A tarefa nao foi editada pois existem campos obrigatorios a serem preenchidos!");
			} else {
				tarefa.setIdProjeto(projeto.getId());
				tarefa.setNome(jTextFieldNome.getText());
				tarefa.setDescricao(jTextAreaDescricao.getText());
				tarefa.setIsCompleto(false);
				tarefa.setId(tarefa.getId());

				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				Date deadline = null;
				deadline = dateFormat.parse(jTextFieldData.getText());
				String dataFormatada = dateFormat.format(deadline);
			
				tarefa.setDeadline(dataFormatada);
				
				controller.update(tarefa);
				JOptionPane.showMessageDialog(this, "Tarefa editada com sucesso!");
				this.dispose();
			}

		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
	}// GEN-LAST:event_jButtonEditarActionPerformed

	private void jTextFieldDataActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jTextFieldDataActionPerformed
		// TODO add your handling code here:
	}// GEN-LAST:event_jTextFieldDataActionPerformed

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		/* Set the Nimbus look and feel */
		// <editor-fold defaultstate="collapsed" desc=" Look and feel setting code
		// (optional) ">
		/*
		 * If Nimbus (introduced in Java SE 6) is not available, stay with the default
		 * look and feel. For details see
		 * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
		 */
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(TarefaView.class.getName()).log(java.util.logging.Level.SEVERE,
					null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(TarefaView.class.getName()).log(java.util.logging.Level.SEVERE,
					null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(TarefaView.class.getName()).log(java.util.logging.Level.SEVERE,
					null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(TarefaView.class.getName()).log(java.util.logging.Level.SEVERE,
					null, ex);
		}
		// </editor-fold>
		// </editor-fold>

		/* Create and display the dialog */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				TarefaView dialog = new TarefaView(new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					@Override
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCalendario;
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JButton jButtonEditar;
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JLabel jLabelDeadline;
    private javax.swing.JLabel jLabelDescricao;
    private javax.swing.JLabel jLabelNome;
    private javax.swing.JLabel jLabelToolBarTitulo;
    private javax.swing.JPanel jPanelTarefa;
    private javax.swing.JPanel jPanelToolBar;
    private javax.swing.JScrollPane jScrollPaneDescricao;
    private javax.swing.JTextArea jTextAreaDescricao;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextFieldData;
    private javax.swing.JTextField jTextFieldNome;
    // End of variables declaration//GEN-END:variables
	

	public void setProjeto(Projetos projeto) {
		this.projeto = projeto;
	}

	void setTarefa(Tarefas tarefa) {
		this.tarefa = tarefa;
	}

}
