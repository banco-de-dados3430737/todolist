/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Date;

/**
 *
 * @author Lucas Matos
 */
public class Projetos {
    private int id;
    private String nome;
    private String descricao;
    private Date criado;
    private Date atualizado;

    public Projetos(int id, String nome, String descricao, Date criado, Date atualizado) {
        this.id = id;
        this.nome = nome;
        this.descricao = descricao;
        this.criado = criado;
        this.atualizado = atualizado;
    }

    public Projetos (){
        this.criado = new Date();
        this.atualizado = new Date();
    }
    // getters and setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getCriado() {
        return criado;
    }

    public void setCriado(Date criado) {
        this.criado = criado;
    }

    public Date getAtualizado() {
        return atualizado;
    }

    public void setAtualizado(Date atualizado) {
        this.atualizado = atualizado;
    }

    @Override
    public String toString() {
        return this.nome;
    }
}
