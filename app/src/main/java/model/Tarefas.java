/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Date;

/**
 *
 * @author Lucas Matos
 */
public class Tarefas {
    private int id;
    private int idProjeto;
    private String nome;
    private String descricao;
    private boolean completo;
    private String deadline;
    private Date criado;
    private Date atualizado;

    //construtores
    public Tarefas(int id, int idProjeto, String nome, String descricao, boolean iscompleto, String deadline, Date criado, Date atualizado) {
        this.id = id;
        this.idProjeto = idProjeto;
        this.nome = nome;
        this.descricao = descricao;
        this.completo = iscompleto;
        this.deadline = deadline;
        this.criado = criado;
        this.atualizado = atualizado;
    }

    public Tarefas(){
          this.completo = false;
        this.criado = new Date();
        this.atualizado = new Date();
    }
            
    //getters and setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdProjeto() {
        return idProjeto;
    }

    public void setIdProjeto(int idProjeto) {
        this.idProjeto = idProjeto;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }


    public boolean isIsCompleto() {
        return completo;
    }

    public void setIsCompleto(boolean iscompleto) {
        this.completo = iscompleto;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public Date getCriado() {
        return criado;
    }

    public void setCriado(Date criado) {
        this.criado = criado;
    }

    public Date getAtualizado() {
        return atualizado;
    }

    public void setAtualizado(Date atualizado) {
        this.atualizado = atualizado;
    }

    @Override
    public String toString() {
        return "Tarefa{" + "id=" + id + ", idProjeto=" + idProjeto + ", nome=" + nome + ", descricao=" + descricao  + ", iscompleto=" + completo + ", deadline=" + deadline + ", criado=" + criado + ", atualizado=" + atualizado + '}';
    }

	
}
