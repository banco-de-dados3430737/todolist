/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import jdbc.ConnectionFactory;
import model.Projetos;

/**
 *
 * @author Lucas Matos
 */
public class ProjetoController {

	private Connection connection;
	private PreparedStatement statement;

	public void create(Projetos projeto) {
		String sql = "INSERT INTO projetos (nome, descricao, createdAt, updatedAt) VALUES (?, ?, ?, ?)";

		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(sql);

			statement.setString(1, projeto.getNome());
			statement.setString(2, projeto.getDescricao());
			statement.setTimestamp(3, new java.sql.Timestamp(System.currentTimeMillis()));
			statement.setTimestamp(4, new java.sql.Timestamp(System.currentTimeMillis()));
			statement.execute();

		} catch (SQLException e) {
			throw new RuntimeException("Nao foi possivel salvar" + e.getMessage());
		} finally {
			ConnectionFactory.closeConnection(connection, statement);
		}

	}

	public void update(Projetos projeto) {
		String sql = "UPDATE projetos SET nome = ?," + "descricao = ?," + "createdAt = ?," + "updatedAt = ? WHERE id=?";

		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(sql);

			statement.setString(1, projeto.getNome());
			statement.setString(2, projeto.getDescricao());
			statement.setTimestamp(3, new java.sql.Timestamp(System.currentTimeMillis()));
			statement.setTimestamp(4, new java.sql.Timestamp(System.currentTimeMillis()));
			statement.setInt(5, projeto.getId());
			statement.execute();

		} catch (Exception e) {
			throw new RuntimeException("Nao foi possovel atualizar o projeto." + e.getMessage());
		} finally {
			ConnectionFactory.closeConnection(connection, statement);
		}
	}

	public void delete(int projetoID) {
		try {
			String sql = "SELECT COUNT(*) FROM tarefas WHERE idProjeto = ?";
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(sql);
			statement.setInt(1, projetoID);
			ResultSet rs = statement.executeQuery();
			rs.next();
			int count = rs.getInt(1);

			if (count > 0) {

				JOptionPane.showMessageDialog(null,
						"Não é possível excluir o projeto, pois existem tarefas associados a ele.");
			} else {
				String deleteSQL = "DELETE FROM projetos WHERE id = ?";
				PreparedStatement deletestatement = connection.prepareStatement(deleteSQL);
				deletestatement.setInt(1, projetoID);
				deletestatement.execute();
				deletestatement.close();

				JOptionPane.showMessageDialog(null, "Produto excluído com sucesso!");
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public List<Projetos> getAll() {
		String sql = "SELECT * FROM projetos";
		List<Projetos> projetos = new ArrayList<>();

		ResultSet resultSet = null;

		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(sql);

			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Projetos projeto = new Projetos();

				projeto.setId(resultSet.getInt("id"));
				projeto.setNome(resultSet.getString("nome"));
				projeto.setDescricao(resultSet.getString("descricao"));
				projeto.setCriado(resultSet.getDate("createdAt"));
				projeto.setAtualizado(resultSet.getDate("updatedAt"));

				projetos.add(projeto);

			}
		} catch (SQLException e) {
			throw new RuntimeException("Erro ao buscar os projetos" + e.getMessage());
		} finally {
			ConnectionFactory.closeConnection(connection, statement, resultSet);
		}

		return projetos;
	}
}
