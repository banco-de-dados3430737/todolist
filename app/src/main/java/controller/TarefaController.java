/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import jdbc.ConnectionFactory;
import model.Tarefas;

/**
 *
 * @author Lucas Matos
 */
public class TarefaController {
        public void create(Tarefas tarefa ){
        String sql = "INSERT INTO tarefas ("
                +"idProjeto,"
                + "nome,"
                + "descricao,"
                + "completo,"
                + "deadline,"
                +"createdAt,"
                + "updatedAt) VALUES(?,?,?,?,?,?,?)";
        
        Connection connection = null;
        PreparedStatement statement = null;
        
        try {
          
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(sql);
            
            statement.setInt(1,tarefa.getIdProjeto());
            statement.setString(2,tarefa.getNome());
            statement.setString(3, tarefa.getDescricao());
            statement.setBoolean(4,tarefa.isIsCompleto());
            statement.setString(5,tarefa.getDeadline());
            statement.setTimestamp(6, new java.sql.Timestamp(System.currentTimeMillis()));
            statement.setTimestamp(7, new java.sql.Timestamp(System.currentTimeMillis()));
            
            statement.execute();
            
        } catch (Exception ex) {
            throw new RuntimeException("Erro ao salvar a tarefa"+ ex.getMessage(),ex);
        } finally {
            ConnectionFactory.closeConnection(connection, statement);
            
        }
    }
    
    public void update (Tarefas tarefa){
        String sql ="UPDATE tarefas SET "
                +"idProjeto = ?, "
                +"nome = ?, "
                +"descricao = ?, "
                +"completo = ?, "
                +"deadline = ?, "
                +"createdAt = ?, "
                +"updatedAt = ? "
                +"WHERE id = ?";
        
        Connection connection = null;
        PreparedStatement statement = null;
        
        try {
     
           
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(sql);

            statement.setInt (1,tarefa.getIdProjeto());
            statement.setString(2, tarefa.getNome());
            statement.setString(3, tarefa.getDescricao());
            statement.setBoolean(4, tarefa.isIsCompleto());
            statement.setString(5, tarefa.getDeadline());
            statement.setTimestamp(6, new java.sql.Timestamp(System.currentTimeMillis()));
            statement.setTimestamp(7, new java.sql.Timestamp(System.currentTimeMillis()));
            statement.setInt(8, tarefa.getId());
            statement.execute();
            
        } catch (SQLException e) {
            throw new RuntimeException("Erro ao atualizar a tarefa"+ e.getMessage());
        } finally{
            ConnectionFactory.closeConnection(connection, statement);
        }
    }
    
    public void delete(int tarefaID) {
        String sql = "DELETE FROM tarefas WHERE id = ?";
        Connection connection = null;
        PreparedStatement statement = null;
        
        try {
            
            connection = ConnectionFactory.getConnection();
            
            statement = connection.prepareStatement(sql);
           
            statement.setInt(1, tarefaID);
            
            statement.execute();
            
        } catch (Exception e) {
            throw new RuntimeException("Erro ao deletar a tarefa");
        } finally { 
            ConnectionFactory.closeConnection(connection, statement);
        }
        
    }
    
    public List<Tarefas> getAll(int IdProjeto){
        String sql = "SELECT * FROM tarefas WHERE idProjeto = ?";
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        
       
        List<Tarefas> tarefas = new ArrayList<Tarefas>();
        
        try {
           
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, IdProjeto);
            resultSet =  statement.executeQuery();
           
            
           while(resultSet.next()){
               Tarefas tarefa = new Tarefas();
               tarefa.setId(resultSet.getInt("id"));
               tarefa.setIdProjeto(resultSet.getInt("idProjeto"));
               tarefa.setNome(resultSet.getString("nome"));
               tarefa.setDescricao(resultSet.getString("descricao"));
               tarefa.setIsCompleto(resultSet.getBoolean("completo"));
               tarefa.setDeadline(resultSet.getString("deadline"));
               tarefa.setCriado(resultSet.getDate("createdAt"));
               tarefa.setAtualizado(resultSet.getDate("updatedAt"));
               
               tarefas.add(tarefa);
           }
        } catch (Exception ex) {
            throw new RuntimeException("Erro ao inserir a tarefa" + ex.getMessage());
        } finally{
            ConnectionFactory.closeConnection(connection, statement, resultSet);
        }
        
        return tarefas ;
    }
}
