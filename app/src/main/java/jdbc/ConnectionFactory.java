/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Lucas Matos
 */
public class ConnectionFactory {
	 
	private static final String URL="jdbc:mysql://localhost/TODOLIST";
    private static final String USER="root";
    private static final String PASSWORD="root";
    
    public static Connection getConnection(){
        try {
            return DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (Exception e) {
            throw new RuntimeException("Erro na conexao com o banco de dados", e);
        }
    }
    
    public static void closeConnection(Connection connection){
        try {
            if ( connection != null){
                connection.close();
            }
        } catch (Exception e) {
            throw new RuntimeException("Erro ao fechar a conex�o com o banco de dados");
          }
    }
    
     public static void closeConnection(Connection connection, PreparedStatement statement){
        try {
            if ( connection != null){
                connection.close();
            }
            if(statement!= null){
                statement.close();
            }
        } catch (Exception e) {
            throw new RuntimeException("Erro ao fechar a conex�o com o banco de dados");
          }
    }
    
        public static void closeConnection(Connection connection, PreparedStatement statement, ResultSet resultSet){
        try {
            if ( connection != null){
                connection.close();
            }
            if(statement!= null){
                statement.close();
            }
            if(resultSet != null){
                resultSet.close();
            }
        } catch (Exception e) {
            throw new RuntimeException("Erro ao fechar a conex�o com o banco de dados");
          }
    }
    
    
}

