/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package util;

import java.awt.Color;
import java.awt.Component;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import model.Tarefas;

/**
 *
 * @author Lucas Matos
 */
public class DeadlineRenderer extends DefaultTableCellRenderer {
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {

		JLabel label;
		label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		label.setHorizontalAlignment(CENTER);

		TabelaTarefas tarefaModel = (TabelaTarefas) table.getModel();
		Tarefas tarefa = tarefaModel.getTarefas().get(row);
		String dataString = tarefa.getDeadline();
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		Date dataTarefa = null;

		try {
			dataTarefa = formato.parse(dataString);
		} catch (ParseException e) {

			e.printStackTrace();
		}

		if (dataTarefa.after(new Date())) {
			label.setBackground(Color.green);
		} else {
			label.setBackground(Color.red);
		}
		return label;
	}
}
