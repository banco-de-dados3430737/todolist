-- Crie o banco de dados e o use
CREATE DATABASE IF NOT EXISTS TODOLIST;
USE TODOLIST;

-- Crie uma tabela para projetos
CREATE TABLE IF NOT EXISTS projetos (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR(255) NOT NULL,
    descricao TEXT,
    createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updatedAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

-- Crie uma tabela para tarefas
CREATE TABLE IF NOT EXISTS tarefas (
    id INT AUTO_INCREMENT PRIMARY KEY,
    idProjeto INT NOT NULL,
    nome VARCHAR(255) NOT NULL,
    descricao TEXT,
    completo BOOLEAN NOT NULL,
    deadline TEXT,
    createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updatedAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (idProjeto) REFERENCES projetos(id)
);

-- Crie um índice na coluna idProjeto para melhorar o desempenho de consultas
CREATE INDEX idx_idProjeto ON tarefas (idProjeto);
 


-- Para fazer um backup manual, use o comando mysqldump:
-- mysqldump -u seu_usuario -p TODOLIST > backup.sql

-- Para restaurar a partir de um backup, use o comando:
-- mysql -u seu_usuario -p TODOLIST < backup.sql
